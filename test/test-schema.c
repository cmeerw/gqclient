
/* vim: set expandtab:sw=4:ts=4 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <lber.h>
#include <ldap.h>
#include <ldap_schema.h>

#ifndef NULL
#define NULL ((void *) 0)
#endif

#ifndef TRUE
#define TRUE ((void *) 1)
#endif


int main(int argc, char *argv[])
{
    BerElement *ptr;
    LDAP *ld;
    LDAPMessage *res, *e;
    LDAP_OBJECT_CLASS *oc;
    int i, msg, parsecode;
    char *attr, **vals, *errp;

    if((ld = ldap_open("localhost", 389)) == NULL)
    {
        printf("ldap_open failed!\n");
        return (1);
    }

    msg = ldap_simple_bind(ld, NULL, NULL);
    if(msg == -1)
    {
        printf("ldap_simple_bind failed: %s\n", ldap_err2string(msg));
        return (1);
    }


    msg = ldap_search_s(ld, "cn=schema", LDAP_SCOPE_BASE, "(objectclass=*)", NULL, 0, &res);

    if(msg == -1)
    {
        printf("ldap_search failed: %s\n", ldap_err2string(msg));
        return (1);
    }

    printf("results\n");

    for(e = ldap_first_entry(ld, res); e; e = ldap_next_entry(ld, e))
    {
        printf("entry\n");
        for(attr = ldap_first_attribute(ld, res, &ptr); attr != NULL;
            attr = ldap_next_attribute(ld, res, ptr))
        {
            printf("attr\n");
            if(!strcasecmp(attr, "objectclasses"))
            {
                vals = ldap_get_values(ld, res, attr);
                if(vals)
                {
                    for(i = 0; vals[i]; i++)
                    {
                        printf("%s\n", vals[i]);
                        oc = ldap_str2objectclass(vals[i], &parsecode, &errp);
                        if(oc)
                        {
                            printf("%s\n", oc->oc_oid);
                        }
                        else
                        {
                            printf("oc is NULL!\n");
                        }
                    }
                }
            }
        }
    }

    ldap_unbind(ld);

    return (0);
}
