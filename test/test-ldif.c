
/* vim: set expandtab:sw=4:ts=4 */

#include <stdio.h>
#include <stdlib.h>

#include <glib.h>

#include <lber.h>
#include <ldap.h>

#include "ldif.h"

#ifndef NULL
#define NULL ((void *) 0)
#endif

#ifndef TRUE
#define TRUE ((void *) 1)
#endif


int main(int argc, char *argv[])
{
    GString *out;
    LDAP *ld;
    LDAPMessage *res, *e;
    int msg;

    if((ld = ldap_open("localhost", 389)) == NULL)
    {
        return (1);
    }

    msg = ldap_simple_bind(ld, NULL, NULL);
    if(msg == -1)
    {
        printf("ldap_simple_bind failed: %s\n", ldap_err2string(msg));
        return (1);
    }


    msg = ldap_search_s(ld, "ou=people, o=house.com", LDAP_SCOPE_SUBTREE,
                        "(cn=lu*)", NULL, 0, &res);

    if(msg == -1)
    {
        printf("ldap_search failed: %s\n", ldap_err2string(msg));
        return (1);
    }

    out = g_string_sized_new(1024);

    for(e = ldap_first_entry(ld, res); e; e = ldap_next_entry(ld, e))
        ldif_entry_out(out, ld, e, 0);

    printf("%s", out->str);

    g_string_free(out, TRUE);

    ldap_unbind(ld);

    return (0);
}
