/* -*- mode: c-mode; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * vim: set expandtab:ts=4:sw=4
    GQ -- a GTK-based LDAP client
    Copyright (C) 1998-2003 Bert Vermeulen
    Copyright (C) 2002-2003 Peter Stamfest

    This program is released under the Gnu General Public License with
    the additional exemption that compiling, linking, and/or using
    OpenSSL is allowed.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef GQ_BROWSE_H_INCLUDED
#define GQ_BROWSE_H_INCLUDED

#include "common.h"
#include "gq-input-form.h"
#include "gq-tree-widget.h"
#include "gq-tab.h"             /* GqTab */

// define to test the new tree view stuff
#warning "get this ready and remove the include from gq-browser-node.h"
#define USE_TREE_VIEW

G_BEGIN_DECLS typedef struct _GqTabBrowse GqTabBrowse;
typedef GqTabClass GqTabBrowseClass;

G_END_DECLS
#include "gq-browser-model.h"
#include "gq-browser-node.h"
    G_BEGIN_DECLS
#define GQ_TYPE_TAB_BROWSE         (gq_tab_browse_get_type())
#define GQ_TAB_BROWSE(i)           (G_TYPE_CHECK_INSTANCE_CAST((i), GQ_TYPE_TAB_BROWSE, GqTabBrowse))
#define GQ_TAB_BROWSE_CLASS(c)     (G_TYPE_CHECK_CLASS_CAST((c), GQ_TYPE_TAB_BROWSE, GqTabBrowseClass))
#define GQ_IS_TAB_BROWSE(i)        (G_TYPE_CHECK_INSTANCE_TYPE((i), GQ_TYPE_TAB_BROWSE))
#define GQ_IS_TAB_BROWSE_CLASS(c)  (G_TYPE_CHECK_CLASS_TYPE((c), GQ_TYPE_TAB_BROWSE))
#define GQ_TAB_BROWSE_GET_CLASS(i) (G_TYPE_INSTANCE_GET_CLASS((i), GQ_TYPE_TAB_BROWSE, GqTabBrowseClass))
    GType gq_tab_browse_get_type(void);
GqTab *gq_tab_browse_new(void);
#warning "FIXME: let everything be displayable withing a GqInputForm and remove _set_content()"
void gq_tab_browse_set_content(GqTabBrowse * self, GtkWidget * content);
void gq_tab_browse_set_input_form(GqTabBrowse * self, GqInputForm * input_form);

struct _GqTabBrowse
{
    GqTab base_instance;

    GList *cur_path;

#warning "FIXME: hide ctreeroot and rename to tree_view"
    GtkWidget *ctreeroot;
#warning "FIXME: remove ctree_refresh"
    GtkWidget *ctree_refresh;
    GtkWidget *mainpane;
#ifndef USE_TREE_VIEW
    GQTreeWidgetNode *tree_row_selected;
#endif

    /* stuff that only gets used temporary, i.e. hacks */
#warning "FIXME: remove these two"
    GtkTreeIter tree_row_popped_up;

    /* lock used to suppress flickering during d'n'd */
    int update_lock;

    /* used to store old hide-button state - Hack */
    int hidden;
};




void record_path(GqTab * tab,
#ifndef USE_TREE_VIEW
                 GQTreeWidget * ctreeroot, GQTreeWidgetNode * node
#else
                 GtkTreeModel * model, GtkTreeIter * iter
#endif
    );



/**************************************************************************/

#ifndef USE_TREE_VIEW
GqServer *server_from_node(GQTreeWidget * ctreeroot, GQTreeWidgetNode * node);
#else
GqServer *server_from_node(GtkTreeModel * model, GtkTreeIter * iter);
#endif

/* void cleanup_browse_mode(GqTab *tab); */

void refresh_subtree(int error_context, GQTreeWidget * ctree, GQTreeWidgetNode * node);

void refresh_subtree_new_dn(int error_context,
                            GQTreeWidget * ctree,
                            GQTreeWidgetNode * node, const char *newdn, int options);

void refresh_subtree_with_options(int error_context,
                                  GQTreeWidget * ctree, GQTreeWidgetNode * node, int options);
#warning "FIXME: this one could take a GqServerDn"
void show_server_dn(int error_context,
                    GtkTreeModel * model, GqServer * server, gchar const *dn, gboolean select_node);
#ifndef USE_TREE_VIEW
GQTreeWidgetNode *show_dn(int error_context,
                          GQTreeWidget * tree, GQTreeWidgetNode * node, const char *dn,
                          gboolean select_node);
#else
void show_dn(int error_context,
             GtkTreeModel * model,
             GtkTreeIter * iter, GtkTreeIter * found, gchar const *dn, gboolean select_node);
#endif

#ifndef USE_TREE_VIEW
GQTreeWidgetNode *node_from_dn(GQTreeWidget * ctreeroot, GQTreeWidgetNode * top, char *dn);
#else
gboolean node_from_dn(GtkTreeModel * model,
                      GtkTreeIter * top, GtkTreeIter * found, gchar const *dn);
#endif

/* void add_single_server(GqTab *tab, GqServer *server); */
#ifndef USE_TREE_VIEW
GQTreeWidgetNode *dn_browse_single_add(GqServer * server, const char *dn,
                                       GQTreeWidget * ctree, GQTreeWidgetNode * node);
#else
void dn_browse_single_add(GqServer * server,
                          gchar const *dn,
                          GtkTreeModel * model, GtkTreeIter * iter, GtkTreeIter * created);
#endif


/* options to the refresh_subtree_with_options functions */

/* NOTE: browse-dnd.h defines additional options starting at 256 */
#warning "FIXME: make this an enumeration"
#define REFRESH_FORCE_EXPAND    1
#define REFRESH_FORCE_UNEXPAND  2

GQTreeWidgetNode *tree_node_from_server_dn(GQTreeWidget * ctree, GqServer * server, const char *dn);

void update_browse_serverlist(GqTab * tab);
void tree_row_close_connection(GqTab * tab);

void set_update_lock(GqTab * tab);
void release_update_lock(GqTab * tab);

G_END_DECLS
#endif
