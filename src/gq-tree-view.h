/* -*- mode: c-mode; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * vim: set expandtab:ts=4:sw=4
 *
 * This file is part of GQ
 *
 * AUTHORS
 *     Sven Herzberg  <herzi@gnome-de.org>
 *
 * Copyright (C) 2007  Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GQ_TREEVIEW_H
#define GQ_TREEVIEW_H

#include <gtk/gtktreeview.h>

G_BEGIN_DECLS typedef GtkTreeView GqTreeView;
typedef GtkTreeViewClass GqTreeViewClass;

#define GQ_TYPE_TREE_VIEW         (gq_tree_view_get_type())

GType gq_tree_view_get_type(void);
GtkWidget *gq_tree_view_new(void);

G_END_DECLS
#endif /* !GQ_TREEVIEW_H */
