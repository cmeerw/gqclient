/* -*- mode: c-mode; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * vim: set expandtab:ts=4:sw=4
 *
 * This file is part of GQ
 *
 * AUTHORS
 *     Sven Herzberg  <herzi@gnome-de.org>
 *
 * Copyright (C) 2006  Sven Herzberg <herzi@gnome-de.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "gq-stack.h"

#include <glib/gmem.h>
#include <glib/gmessages.h>
#include <glib/glist.h>

#undef DEBUG_STACK

struct _GqStack
{
    GList *list;
};

GqStack *gq_stack_new(void)
{
    return g_new0(GqStack, 1);
}

guint gq_stack_get_n_elements(GqStack const *self)
{
    g_return_val_if_fail(self, 0);
    return g_list_length(self->list);
}

gpointer gq_stack_get_element(GqStack const *self, guint index)
{
    g_return_val_if_fail(self, NULL);

    return g_list_nth_data(self->list, index);
}

void gq_stack_push(GqStack * self, gpointer data)
{
#ifdef DEBUG_STACK
    g_message("gq_stack_push(%p, %p)", self, data);
#endif
    g_return_if_fail(self);

    self->list = g_list_prepend(self->list, data);
}

void gq_stack_raise(GqStack * self, gconstpointer data)
{
    GList *node = NULL;
#ifdef DEBUG_STACK
    g_message("gq_stack_raise(%p, %p)", self, data);
#endif
    g_return_if_fail(self);

    node = g_list_find(self->list, data);
    g_return_if_fail(node);

    self->list = g_list_remove_link(self->list, node);
    self->list = g_list_concat(node, self->list);
}

void gq_stack_remove(GqStack * self, gconstpointer data)
{
    GList *node = NULL;
#ifdef DEBUG_STACK
    g_message("gq_stack_remove(%p, %p)", self, data);
#endif
    g_return_if_fail(self);

    node = g_list_find(self->list, data);
    g_return_if_fail(node);

    self->list = g_list_delete_link(self->list, node);
}
