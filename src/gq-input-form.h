/* -*- mode: c-mode; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * vim: set expandtab:ts=4:sw=4
    GQ -- a GTK-based LDAP client
    Copyright (C) 1998-2003 Bert Vermeulen
    Copyright (C) 2002-2003 Peter Stamfest
    Copyright (C) 2006      Sven Herzberg

    This program is released under the Gnu General Public License with
    the additional exemption that compiling, linking, and/or using
    OpenSSL is allowed.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef GQ_INPUT_FORM_H
#define GQ_INPUT_FORM_H

#include <glib.h>
#include <gtk/gtk.h>
#include "common.h"
#include "gq-formfill.h"
#include "gq-server-dn.h"

G_BEGIN_DECLS typedef struct _GqInputForm GqInputForm;
typedef GtkVBoxClass GqInputFormClass;

#define GQ_TYPE_INPUT_FORM         (gq_input_form_get_type())
#define GQ_INPUT_FORM(i)           (G_TYPE_CHECK_INSTANCE_CAST((i), GQ_TYPE_INPUT_FORM, GqInputForm))
#define GQ_INPUT_FORM_CLASS(c)     (G_TYPE_CHECK_CLASS_CAST((c), GQ_TYPE_INPUT_FORM, GqInputFormClass))
#define GQ_IS_INPUT_FORM(i)        (G_TYPE_CHECK_INSTANCE_TYPE((i), GQ_TYPE_INPUT_FORM))
#define GQ_IS_INPUT_FORM_CLASS(c)  (G_TYPE_CHECK_CLASS_TYPE((c), GQ_TYPE_INPUT_FORM))
#define GQ_INPUT_FORM_GET_CLASS(i) (G_TYPE_INSTANCE_GET_CLASS((i), GQ_TYPE_INPUT_FORM, GqInputFormClass))

GType gq_input_form_get_type(void);
GtkWidget *gq_input_form_new(void);

GqTreeWidgetNode *gq_input_form_get_ctree_refresh(GqInputForm const *self);
void gq_input_form_set_ctree_refresh(GqInputForm * self, GqTreeWidgetNode * node);

GqTreeWidget *gq_input_form_get_ctree_root(GqInputForm const *self);
void gq_input_form_set_ctree_root(GqInputForm * self, GqTreeWidget * widget);

#warning "FIXME: remove these two"
gchar const *gq_input_form_get_dn(GqInputForm const *self);
GqServer *gq_input_form_get_server(GqInputForm const *self);

gboolean gq_input_form_get_editable(GqInputForm const *self);
void gq_input_form_set_editable(GqInputForm * self, gboolean editable);

GqServerDn *gq_input_form_get_entry(GqInputForm const *self);
void gq_input_form_set_entry(GqInputForm * self, GqServerDn * entry);

GList *gq_input_form_get_formlist(GqInputForm const *self);
void gq_input_form_set_formlist(GqInputForm * self, GList * formlist);

gboolean gq_input_form_get_hide(GqInputForm const *self);
void gq_input_form_set_hide(GqInputForm * self, gboolean hide);

gchar const *gq_input_form_get_old_dn(GqInputForm const *self);
void gq_input_form_set_old_dn(GqInputForm * self, gchar const *old_dn);

GList *gq_input_form_get_oldlist(GqInputForm const *self);
void gq_input_form_set_oldlist(GqInputForm * self, GList * oldlist);

GtkWidget *gq_input_form_get_scrolled_window(GqInputForm const *self);

struct _GqInputForm
{
    GtkVBox base_instance;

    GtkWidget *parent_window;
    GtkWidget *hide_attr_button;
    GtkWidget *dn_widget;
    GtkWidget *selected_ctree_refresh;
    void (*activate) (GqInputForm * self, GtkWidget * dn_entry);
    GqFormfill *focusform;
    GtkTooltips *tooltips;
};

struct jumptable
{
    char *jumpstring;
    void *jumpfunc;
};

/* Maybe we will align attribute labels differently in the future.. */
#define LABEL_JUSTIFICATION 0.0
#define CONTAINER_BORDER_WIDTH  6

void save_input_snapshot(int error_context, GqInputForm * iform, const char *state_name);
void restore_input_snapshot(int error_context, GqInputForm * iform, const char *state_name);
void create_form_window(GqInputForm * form);
void build_or_update_inputform(int error_context, GqInputForm * form, gboolean build);
void edit_entry(GqServer * server, const char *dn);
void new_from_entry(GqServer * server, const char *dn);
void update_formlist(GqInputForm * iform);
void clear_table(GqInputForm * iform);
void mod_entry_from_formlist(GqInputForm * iform);
int change_rdn(GqInputForm * iform, int context);
LDAPMod **formdiff_to_ldapmod(GList * oldlist, GList * newlist);
char **glist_to_mod_values(GList * values);
struct berval **glist_to_mod_bvalues(GList * values);
int find_value(GList * list, GByteArray * value);
void destroy_editwindow(GqInputForm * iform);
void add_row(GtkWidget * button, GqInputForm * form);
GtkWidget *gq_new_arrowbutton(GqInputForm * iform);
GtkWidget *find_focusbox(GList * formlist);
void set_hide_empty_attributes(int hidden, GqInputForm * form);

GdkWindow *get_any_gdk_window(GtkWidget * w);

G_END_DECLS
#endif
