/* -*- mode: c-mode; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * vim: set expandtab:ts=4:sw=4
    GQ -- a GTK-based LDAP client
    Copyright (C) 1998-2003 Bert Vermeulen
    Copyright (C) 2002-2003 Peter Stamfest
    Copyright (C) 2006      Sven Herzberg

    This program is released under the Gnu General Public License with
    the additional exemption that compiling, linking, and/or using
    OpenSSL is allowed.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef GQ_SEARCH_H_INCLUDED
#define GQ_SEARCH_H_INCLUDED

#include "common.h"
#include "gq-server-dn.h"
#include "gq-tab.h"

G_BEGIN_DECLS typedef GqTab GqTabSearch;
typedef GqTabClass GqTabSearchClass;

#define GQ_TYPE_TAB_SEARCH         (gq_tab_search_get_type())
#define GQ_TAB_SEARCH(i)           (G_TYPE_CHECK_INSTANCE_CAST((i), GQ_TYPE_TAB_SEARCH, GqTabSearch))
#define GQ_TAB_SEARCH_CLASS(c)     (G_TYPE_CHECK_CLASS_CAST((c), GQ_TYPE_TAB_SEARCH, GqTabSearchClass))
#define GQ_IS_TAB_SEARCH(i)        (G_TYPE_CHECK_INSTANCE_TYPE((i), GQ_TYPE_TAB_SEARCH))
#define GQ_IS_TAB_SEARCH_CLASS(c)  (G_TYPE_CHECK_CLASS_TYPE((c), GQ_TYPE_TAB_SEARCH))
#define GQ_TAB_SEARCH_GET_CLASS(i) (G_TYPE_INSTANCE_GET_CLASS((i), GQ_TYPE_TAB_SEARCH, GqTabSearchClass))

GType gq_tab_search_get_type(void);

typedef enum
{
    GQ_SEARCH_MODE_BEGINS_WITH,
    GQ_SEARCH_MODE_ENDS_WITH,
    GQ_SEARCH_MODE_CONTAINS,
    GQ_SEARCH_MODE_EQUALS
} GqSearchMode;

GqTab *gq_tab_search_new(void);
gchar const *gq_tab_search_get_base_dn(GqTabSearch const *self);
void gq_tab_search_set_base_dn(GqTabSearch * self, gchar const *base_dn);
GqSearchMode gq_tab_search_get_mode(GqTabSearch const *self);
GqServer *gq_tab_search_get_server(GqTabSearch const *self);
void gq_tab_search_set_server(GqTabSearch * self, GqServer * server);

char *make_filter(GqServer const *server, char const *querystring, GqSearchMode mode);

void fill_out_search(GqTab * tab, GqServer * server, const char *search_base_dn);

G_END_DECLS
#endif /* GQ_SEARCH_H_INCLUDED */
