/* -*- mode: c-mode; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * vim: set expandtab:ts=4:sw=4
 *
 * This file is part of GQ
 *
 * AUTHORS
 *     Sven Herzberg  <herzi@gnome-de.org>
 *
 * Copyright (C) 2006  Sven Herzberg <herzi@gnome-de.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GQ_STACK_H
#define GQ_STACK_H

#include <glib/gtypes.h>

G_BEGIN_DECLS typedef struct _GqStack GqStack;

GqStack *gq_stack_new(void);
#if 0
void gq_stack_free(GqStack * self);

gpointer gq_stack_peek(GqStack const *self);
gpointer gq_stack_pop(GqStack * self);
#endif
void gq_stack_push(GqStack * self, gpointer data);
void gq_stack_remove(GqStack * self, gconstpointer data);

guint gq_stack_get_n_elements(GqStack const *self);
gpointer gq_stack_get_element(GqStack const *self, guint index);

void gq_stack_raise(GqStack * self, gconstpointer data);

G_END_DECLS
#endif /* !GQ_STACK_H */
