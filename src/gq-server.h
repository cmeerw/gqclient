/* -*- mode: c-mode; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * vim: set expandtab:ts=4:sw=4
    GQ -- a GTK-based LDAP client
    Copyright (C) 1998-2003 Bert Vermeulen
    Copyright (C) 2002-2003 Peter Stamfest
    Copyright (C) 2006      Sven Herzberg

    This program is released under the Gnu General Public License with
    the additional exemption that compiling, linking, and/or using
    OpenSSL is allowed.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef GQ_SERVER_H
#define GQ_SERVER_H

#include <glib-object.h>
#include "gq-ldap.h"

G_BEGIN_DECLS typedef struct _GqServer GqServer;
typedef GObjectClass GqServerClass;

#define GQ_TYPE_SERVER        (gq_server_get_type())
#define GQ_SERVER(i)          (G_TYPE_CHECK_INSTANCE_CAST((i), GQ_TYPE_SERVER, GqServer))
#define GQ_IS_SERVER(i)       (G_TYPE_CHECK_INSTANCE_TYPE((i), GQ_TYPE_SERVER))

struct server_schema
{
    GList *oc;
    GList *at;
    GList *mr;
    GList *s;
};

typedef enum
{
    GQ_BIND_SIMPLE = 0,
    GQ_BIND_KERBEROS = 1,
    GQ_BIND_SASL = 2
} GqBindType;

GType gq_server_get_type(void);
GqServer *gq_server_new(gchar const *name);

void gq_server_copy(GqServer const *self, GqServer * target);

gboolean gq_server_get_ask_pw(GqServer const *self);
void gq_server_set_ask_pw(GqServer * self, gboolean ask_pw);

gchar const *gq_server_get_bind_dn(GqServer const *self);
void gq_server_set_bind_dn(GqServer * self, gchar const *bind_dn);

GqBindType gq_server_get_bind_type(GqServer const *self);
void gq_server_set_bind_type(GqServer * self, GqBindType bind_type);

gchar const *gq_server_get_saslmechanism(GqServer const *self);
void gq_server_set_saslmechanism(GqServer * self, gchar const *bind_dn);

gboolean gq_server_get_cache_connection(GqServer const *self);
void gq_server_set_cache_connection(GqServer * self, gboolean cache_connection);

gboolean gq_server_is_connected(GqServer const *self);
void gq_server_set_connected(GqServer * self, gboolean is_connected);

LDAP *gq_server_get_connection(GqServer const *self);

gboolean gq_server_get_hide_internals(GqServer const *self);
void gq_server_set_hide_internals(GqServer * self, gboolean hide_internals);

gchar const *gq_server_get_host(GqServer const *self);
void gq_server_set_host(GqServer * self, gchar const *host);

gchar const *gq_server_get_name(GqServer const *self);
void gq_server_set_name(GqServer * self, gchar const *name);

gint gq_server_get_port(GqServer const *self);
void gq_server_set_port(GqServer * self, gint port);

gchar const *gq_server_get_search_attribute(GqServer const *self);
void gq_server_set_search_attribute(GqServer * self, gchar const *search_attribute);

gboolean gq_server_get_use_tls(GqServer const *self);
void gq_server_set_use_tls(GqServer * self, gboolean use_tls);

void free_ldapserver(GqServer * server);

/** NOTE: copy_ldapserver ONLY copies the configuration
    information. It DOES NOT copy the operational stuff. Often you
    will want to call reset_ldapserver(target) alongside of
    copy_ldapserver.  */
void copy_ldapserver(GqServer * target, const GqServer * source);

/** NOTE: reset_ldapserver sets the target refcount to 0 */
void reset_ldapserver(GqServer * target);

/* canonicalize_ldapserver - to be called whenever the server-related
   information gets changed for the server in order to recalculate
   some dependent information. Eg. a change to the ldaphost might
   change the fact that a server get specified via an ldap URI or
   not. Another example is the change of the ldaphost causing a change
   to the canonical name of the server. This is where the name
   originated. */
void canonicalize_ldapserver(GqServer * server);

typedef enum
{
    SERVER_HAS_NO_SCHEMA = 1
} GqServerFlags;

struct _GqServer
{
    GObject base_instance;

    char *basedn;
    char *saslmechanism;
    long local_cache_timeout;
    int show_ref;               /* obsolete - kept for configfile compatibility */

    /* the canonical name of the host. Essentially this is the
     * corresponding LDAP URI for the ldaphost/port combination -
     * maintained through canonicalize_ldapserver() */
    char *canon_name;

    /* a flag indicating if ldaphost seems to be a URI or not */
    int is_uri;

    /* if quiet is non-zero open_connection will not pop-up any error
     * or questions */
    int quiet;

    /* internal data */

    int incarnation;            /* number of bind operations done so far,
                                 * never decremented - this is a purely
                                 * statistical number */
    int missing_closes;         /* incremented on every open_connection,
                                 * decremented on each close,
                                 * close_connection really closes only if
                                 * this drops to zero */
    struct server_schema *ss;
    int flags;

    int version;
    /* server_down is a flag set by the SIGPIPE signal handler and in
     * case of an LDAP_SERVER_DOWN error. It indicates that we should
     * reconnect the next time open_connection gets called. There is
     * no (simple) way to find out which connection has been broken
     * in case of a SIGPIPE, thus we have to record this for every
     * connection. We MIGHT instead check for ld_errno in the LDAP
     * structure, but that is neither really documented (some man
     * pages mention it though) nor is it actually available through
     * ldap.h */
    int server_down;
};

G_END_DECLS
#endif /* !GQ_SERVER_H */
