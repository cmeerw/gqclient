/* -*- mode: c-mode; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * vim: set expandtab:ts=4:sw=4
 *
 * This file is part of GQ
 *
 * AUTHORS
 *     Sven Herzberg  <herzi@gnome-de.org>
 *
 * Copyright (C) 2006  Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "gq-browser-node-dummy.h"

GqBrowserNode *gq_browser_node_dummy_new(void)
{
    return g_object_new(GQ_TYPE_BROWSER_NODE_DUMMY, NULL);
}

/* GType */
G_DEFINE_TYPE(GqBrowserNodeDummy, gq_browser_node_dummy, GQ_TYPE_BROWSER_NODE);

static void gq_browser_node_dummy_init(GqBrowserNodeDummy * self)
{
    gq_browser_node_set_seen(GQ_BROWSER_NODE(self), TRUE);
}

static gchar *browser_node_dummy_get_name(GqBrowserNode const *node G_GNUC_UNUSED,
                                          gboolean long_desc G_GNUC_UNUSED)
{
    return g_strdup("Dummy Node");
}

static void gq_browser_node_dummy_class_init(GqBrowserNodeDummyClass * self_class)
{
    GqBrowserNodeClass *node_class = GQ_BROWSER_NODE_CLASS(self_class);
    node_class->get_name = browser_node_dummy_get_name;
}
