/* This file is part of GQ
 *
 * AUTHORS
 *     Sven Herzberg  <herzi@gnome-de.org>
 *
 * Copyright (C) 2006  Sven Herzberg <herzi@gnome-de.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#import "gq-keyring.h"

gboolean
gq_keyring_can_save (void)
{
	return FALSE;
}

void
gq_keyring_forget_password (GqServer const* server)
{}

void
gq_keyring_free_password (gchar* password)
{
	g_free (password);
}

gchar*
gq_keyring_get_password (GqServer const* server)
{
	return NULL;
}

void
gq_keyring_save_password (GqServer const* server,
			  gchar const   * password)
{}

