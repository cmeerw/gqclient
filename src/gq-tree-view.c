/* -*- mode: c-mode; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * vim: set expandtab:ts=4:sw=4
 *
 * This file is part of GQ
 *
 * AUTHORS
 *     Sven Herzberg  <herzi@gnome-de.org>
 *
 * Copyright (C) 2007  Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "gq-tree-view.h"

#include "gq-main-loop.h"

struct GqTreeViewPrivate
{
    guint source_id;
};
#define P(i) (G_TYPE_INSTANCE_GET_PRIVATE((i), GQ_TYPE_TREE_VIEW, struct GqTreeViewPrivate))

GtkWidget *gq_tree_view_new(void)
{
    return g_object_new(GQ_TYPE_TREE_VIEW, NULL);
}

/* GType */
G_DEFINE_TYPE(GqTreeView, gq_tree_view, GTK_TYPE_TREE_VIEW);

static void gq_tree_view_init(GqTreeView * self G_GNUC_UNUSED)
{
}

static void tree_view_dispose(GObject * object)
{
    if(P(object)->source_id)
    {
        g_source_remove(P(object)->source_id);
        P(object)->source_id = 0;
    }
    G_OBJECT_CLASS(gq_tree_view_parent_class)->dispose(object);
}

static gboolean tree_view_queue_draw(gpointer data)
{
    if(!gq_main_loop_blocked())
    {
        GtkWidget *widget = GTK_WIDGET(data);
        gtk_widget_queue_draw(widget);
        P(widget)->source_id = 0;
        return FALSE;
    }

    return TRUE;
}

static gboolean tree_view_expose_event(GtkWidget * widget, GdkEventExpose * event)
{
    if(gq_main_loop_blocked() && !P(widget)->source_id)
    {
        P(widget)->source_id = g_timeout_add(100, tree_view_queue_draw, widget);
        return TRUE;
    }
    else if(!gq_main_loop_blocked())
    {
        return GTK_WIDGET_CLASS(gq_tree_view_parent_class)->expose_event(widget, event);
    }
    return FALSE;
}

static void gq_tree_view_class_init(GqTreeViewClass * self_class)
{
    GObjectClass *object_class = G_OBJECT_CLASS(self_class);
    GtkWidgetClass *widget_class = GTK_WIDGET_CLASS(self_class);

    object_class->dispose = tree_view_dispose;
    widget_class->expose_event = tree_view_expose_event;

    g_type_class_add_private(self_class, sizeof(struct GqTreeViewPrivate));
}
