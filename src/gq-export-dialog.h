/* -*- mode: c-mode; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * vim: set expandtab:ts=4:sw=4
 *
 * This file is part of GQ
 *
 * AUTHORS
 *     Sven Herzberg
 *
 * Copyright (C) 1998-2003 Bert Vermeulen
 * Copyright (C) 2002-2003 Peter Stamfest
 * Copyright (C) 2006  Sven Herzberg
 *
 * This program is released under the Gnu General Public License with
 * the additional exemption that compiling, linking, and/or using
 * OpenSSL is allowed.
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License as
 * published by the Free Software Foundation; either version 2 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GQ_BROWSE_EXPORT_H_INCLUDED
#define GQ_BROWSE_EXPORT_H_INCLUDED

#include <gtk/gtkwindow.h>

G_BEGIN_DECLS
    void gq_export_server_dns(int error_context G_GNUC_UNUSED,
                              GtkWindow * parent, GList * to_export);

G_END_DECLS
#endif
