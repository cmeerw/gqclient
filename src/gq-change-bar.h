/* -*- mode: c-mode; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * vim: set expandtab:ts=4:sw=4
 *
 * This file is part of GQ
 *
 * AUTHORS
 *     Sven Herzberg  <herzi@gnome-de.org>
 *
 * Copyright (C) 2006  Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GQ_CHANGE_BAR_H
#define GQ_CHANGE_BAR_H

#include <gtk/gtkdrawingarea.h>
#include "gq-comparison.h"

G_BEGIN_DECLS typedef GtkDrawingArea GqChangeBar;
typedef GtkDrawingAreaClass GqChangeBarClass;

#define GQ_TYPE_CHANGE_BAR         (gq_change_bar_get_type())
#define GQ_CHANGE_BAR(i)           (G_TYPE_CHECK_INSTANCE_CAST((i), GQ_TYPE_CHANGE_BAR, GqChangeBar))
#define GQ_CHANGE_BAR_CLASS(c)     (G_TYPE_CHECK_CLASS_CAST((c), GQ_TYPE_CHANGE_BAR, GqChangeBarClass))
#define GQ_IS_CHANGE_BAR(i)        (G_TYPE_CHECK_INSTANCE_TYPE((i), GQ_TYPE_CHANGE_BAR))
#define GQ_IS_CAHNGE_BAR_CLASS(c)  (G_TYPE_CHECK_CLASS_TYPE((c), GQ_TYPE_CHANGE_BAR))
#define GQ_CHANGE_BAR_GET_CLASS(i) (G_TYPE_INSTANCE_GET_CLASS((i), GQ_TYPE_CHANGE_BAR, GqChangeBarClass))

GType gq_change_bar_get_type(void);
GtkWidget *gq_change_bar_new(GqComparison * compare, GqInputForm * form);

G_END_DECLS
#endif /* !GQ_CHANGE_BAR_H */
