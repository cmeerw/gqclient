/* -*- mode: c-mode; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * vim: set expandtab:ts=4:sw=4
 *
 * This file is part of GQ
 *
 * AUTHORS
 *     Sven Herzberg  <herzi@gnome-de.org>
 *
 * Copyright (C) 2006  Sven Herzberg <herzi@gnome-de.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GQ_SERVER_DIALOG_H
#define GQ_SERVER_DIALOG_H

#include "gq-server.h"
#include "herzi-glade-dialog.h"

G_BEGIN_DECLS typedef HerziGladeDialog GqServerDialog;
typedef HerziGladeDialogClass GqServerDialogClass;

#define GQ_TYPE_SERVER_DIALOG         (gq_server_dialog_get_type())
#define GQ_SERVER_DIALOG(i)           (G_TYPE_CHECK_INSTANCE_CAST((i), GQ_TYPE_SERVER_DIALOG, GqServerDialog))
#define GQ_SERVER_DIALOG_CLASS(c)     (G_TYPE_CHECK_CLASS_CAST((c), GQ_TYPE_SERVER_DIALOG, GqServerDialogClass))
#define GQ_IS_SERVER_DIALOG(i)        (G_TYPE_CHECK_INSTANCE_TYPE((i), GQ_TYPE_SERVER_DIALOG))
#define GQ_IS_SERVER_DIALOG_CLASS(c)  (G_TYPE_CHECK_CLASS_TYPE((c), GQ_TYPE_SERVER_DIALOG))
#define GQ_SERVER_DIALOG_GET_CLASS(i) (G_TYPE_INSTANCE_GET_CLASS((i), GQ_TYPE_SERVER_DIALOG, GqServerDialogClass))

GType gq_server_dialog_get_type(void);
GtkWidget *gq_server_dialog_new(GqServer * server, GtkWindow * parent);

GqServer *gq_server_dialog_get_server(GqServerDialog const *self);

G_END_DECLS
#endif /* !GQ_SERVER_DIALOG_H */
