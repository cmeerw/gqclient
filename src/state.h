/* -*- mode: c-mode; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * vim: set expandtab:ts=4:sw=4
    GQ -- a GTK-based LDAP client
    Copyright (C) 1998-2003 Bert Vermeulen
    Copyright (C) 2002-2003 by Peter Stamfest

    This program is released under the Gnu General Public License with
    the additional exemption that compiling, linking, and/or using
    OpenSSL is allowed.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef GQ_STATE_H
#define GQ_STATE_H

#include <gtk/gtk.h>

G_BEGIN_DECLS
    GtkWidget * stateful_gtk_window_new(GtkWindowType type, const char *name, int w, int h);

void init_state(void);
void save_state(void);

/* Never use non-ASCII entity names */
struct state_entity *lookup_entity(const char *entity_name);
void rm_value(const char *state_name);

int state_value_get_enum(gchar const *state_name,
                         gchar const *value_name, gint default_value, GType enum_type);
void state_value_set_enum(gchar const *state_name,
                          gchar const *value_name, gint value, GType enum_type);
int state_value_get_int(const char *state_name, const char *value_name, int def);
void state_value_set_int(const char *state_name, const char *value_name, int n);

const gchar *state_value_get_string(const char *state_name,
                                    const char *value_name, const char *def);
void state_value_set_string(const char *state_name, const char *value_name, const char *c);

GType state_value_get_type(gchar const *state_name, gchar const *value_name, GType default_type);
void state_value_set_type(gchar const *state_name, gchar const *value_name, GType type);

const GList *state_value_get_list(const char *state_name, const char *value_name);

/* convenience functions for common list manipulations */
GList *copy_list_of_strings(const GList * src);
GList *free_list_of_strings(GList * l);

/* The list set her MUST be a list of strings - nothing else will work */
void state_value_set_list(const char *state_name, const char *value_name, const GList * n);


gboolean exists_entity(const char *entity_name);

G_END_DECLS
#endif
