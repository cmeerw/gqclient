/* -*- mode: c-mode; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * vim: set expandtab:ts=4:sw=4
    GQ -- a GTK-based LDAP client
    Copyright (C) 1998-2003 Bert Vermeulen
    Copyright (C) 2002-2003 Peter Stamfest

    This program is released under the Gnu General Public License with
    the additional exemption that compiling, linking, and/or using
    OpenSSL is allowed.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef GQ_FORMFILL_H
#define GQ_FORMFILL_H

#include <ldap_schema.h>

#ifdef HAVE_CONFIG_H
# include  <config.h>           /* pull in HAVE_* defines */
#endif /* HAVE_CONFIG_H */

#include "common.h"

G_BEGIN_DECLS typedef GObject GqFormfill;
typedef GObjectClass GqFormfillClass;

G_END_DECLS
#include "syntax.h"
    G_BEGIN_DECLS
#define GQ_TYPE_FORMFILL         (gq_formfill_get_type())
#define GQ_FORMFILL(i)           (G_TYPE_CHECK_INSTANCE_CAST((i), GQ_TYPE_FORMFILL, GqFormfill))
#define GQ_FORMFILL_CLASS(c)     (G_TYPE_CHECK_CLASS_CAST((c), GQ_TYPE_FORMFILL, GqFormfillClass))
#define GQ_IS_FORMFILL(i)        (G_TYPE_CHECK_INSTANCE_TYPE((i), GQ_TYPE_FORMFILL))
#define GQ_IS_FORMFILL_CLASS(c)  (G_TYPE_CHECK_CLASS_CAST((c), GQ_TYPE_FORMFILL))
#define GQ_FORMFILL_GET_CLASS(i) (G_TYPE_INSTANCE_GET_CLASS((i), GQ_TYPE_FORMFILL, GqFormfillClass))
    typedef enum
{
    // FIXME: add DISPLAYTYPE_INVALID = 0,
    DISPLAYTYPE_DN = 1,
    GQ_DISPLAY_TYPE_ENTRY,
    DISPLAYTYPE_TEXT = 3,
    DISPLAYTYPE_PASSWORD = 4,
    DISPLAYTYPE_BINARY = 5,
    DISPLAYTYPE_JPEG = 6,
    DISPLAYTYPE_OC = 7,
#ifdef HAVE_LIBCRYPTO
    DISPLAYTYPE_CERT = 8,
    DISPLAYTYPE_CRL = 9,
#else
    DISPLAYTYPE_CERT = DISPLAYTYPE_BINARY,
    DISPLAYTYPE_CRL = DISPLAYTYPE_BINARY,
#endif
    DISPLAYTYPE_TIME = 10,
    DISPLAYTYPE_INT = 11,
    DISPLAYTYPE_NUMSTR = 12,
    DISPLAYTYPE_DATE = 13
} GQDisplayType;
#warning "FIXME: call them right"

typedef GQDisplayType GqDisplayType;

#warning "FIXME: make flags from these"
#define FLAG_NOT_IN_SCHEMA      0x01
#define FLAG_MUST_IN_SCHEMA 0x02

/* The FLAG_DEL_ME is used to mark form entries not compatible with
   the schema of the object */
#define FLAG_DEL_ME     0x04

/* Used to suppress the "more" button for single valued attributes */
#define FLAG_SINGLE_VALUE   0x08

/* Used to temporarily mark attributes added for extensibleObject entries */
#define FLAG_EXTENSIBLE_OBJECT_ATTR 0x10

/* Used to disable widgets for attributes marked as no_user_mod */
#define FLAG_NO_USER_MOD    0x80

/* forward decls to avoid circular inclusion problems */
#warning "FIXME: remove these forward decls"
struct _display_type_handler;

#warning "FIXME: remove aliases"
#define DISPLAYTYPE_ENTRY GQ_DISPLAY_TYPE_ENTRY
#define new_formfill() gq_formfill_new()
#define free_formfill(i) g_object_unref(i)

GType gq_formfill_get_type(void);
GqFormfill *gq_formfill_new(void);

gboolean gq_formfill_equals_data(GqFormfill * form1, GqFormfill * form2);

gchar const *gq_formfill_get_attrname(GqFormfill const *self);
void gq_formfill_set_attrname(GqFormfill * self, gchar const *attrname);

GqDisplayType gq_formfill_get_display_type(GqFormfill const *self);
void gq_formfill_set_display_type(GqFormfill * self, GqDisplayType type);

GType gq_formfill_get_dt_handler(GqFormfill const *self);
void gq_formfill_change_display_type(GqFormfill * self, GType type);
void gq_formfill_set_dt_handler(GqFormfill * self, GType gtype);

GtkWidget *gq_formfill_get_event_box(GqFormfill const *self);
void gq_formfill_set_event_box(GqFormfill * self, GtkWidget * event_box);

int gq_formfill_get_flags(GqFormfill const *self);
void gq_formfill_set_flag(GqFormfill * self, int flag);
void gq_formfill_unset_flag(GqFormfill * self, int flag);

GtkWidget *gq_formfill_get_label(GqFormfill const *self);
void gq_formfill_set_label(GqFormfill * self, GtkWidget * label);

GtkWidget *gq_formfill_get_morebutton(GqFormfill const *self);
void gq_formfill_set_morebutton(GqFormfill * self, GtkWidget * morebutton);

int gq_formfill_get_n_inputfields(GqFormfill const *self);
void gq_formfill_set_n_inputfields(GqFormfill * self, int n_fields);

GqServer *gq_formfill_get_server(GqFormfill const *self);
void gq_formfill_set_server(GqFormfill * self, GqServer * server);

GqSyntax *gq_formfill_get_syntax(GqFormfill const *self);
void gq_formfill_set_syntax(GqFormfill * self, GqSyntax * syntax);

GList *gq_formfill_get_values(GqFormfill const *self);
void gq_formfill_add_value(GqFormfill * self, GByteArray * value);

GtkWidget *gq_formfill_get_vbox(GqFormfill const *self);
void gq_formfill_set_vbox(GqFormfill * self, GtkWidget * vbox);

GList *gq_formfill_get_widgets(GqFormfill const *self);
void gq_formfill_add_widget(GqFormfill * self, GtkWidget * widget);
void gq_formfill_clear_widgets(GqFormfill * self);

#warning "FIXME: (un)init the internal attrs from the class"
void init_internalAttrs(void);
gboolean isInternalAttr(const char *attr);

void free_formlist(GList * formlist);
void free_formfill_values(GqFormfill * form);
GList *formlist_append(GList * formlist, GqFormfill * form);
#warning "FIXME: formfill_from_entry() should get a GqServerDn"
GList *formlist_from_entry(int error_context, GqServer * server, const char *dn, int ocvalues_only);
GList *dup_formlist(GList * formlist);
void dump_formlist(GList * formlist);
GqFormfill *lookup_attribute(GList * formlist, gchar const *attr);
GqFormfill *lookup_attribute_using_schema(GList * formlist,
                                          const char *attr,
                                          struct server_schema *schema,
                                          LDAPAttributeType ** attrtype);
int find_displaytype(int error_context, GqServer * server, GqFormfill * form);
void set_displaytype(int error_context, GqServer * server, GqFormfill * form);

char *attr_strip(const char *attr);

G_END_DECLS
#endif /* GQ_FORMFILL_H */
