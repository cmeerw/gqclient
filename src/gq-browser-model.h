/* -*- mode: c-mode; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * vim: set expandtab:ts=4:sw=4
 *
 * This file is part of GQ
 *
 * AUTHORS
 *     Sven Herzberg  <herzi@gnome-de.org>
 *
 * Copyright (C) 2006  Sven Herzberg <herzi@gnome-de.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GQ_BROWSER_MODEL_H
#define GQ_BROWSER_MODEL_H

#include <gtk/gtktreemodel.h>

G_BEGIN_DECLS typedef GObject GqBrowserModel;
typedef GObjectClass GqBrowserModelClass;

G_END_DECLS
#include "gq-browser-node.h"
    G_BEGIN_DECLS
#define GQ_TYPE_BROWSER_MODEL         (gq_browser_model_get_type())
#define GQ_BROWSER_MODEL(i)           (G_TYPE_CHECK_INSTANCE_CAST((i), GQ_TYPE_BROWSER_MODEL, GqBrowserModel))
#define GQ_BROWSER_MODEL_CLASS(c)     (G_TYPE_CHECK_CLASS_CAST((c), GQ_TYPE_BROWSER_MODEL, GqBrowserModelClass))
#define GQ_IS_BROWSER_MODEL(i)        (G_TYPE_CHECK_INSTANCE_TYPE((i), GQ_TYPE_BROWSER_MODEL))
#define GQ_IS_BROWSER_MODEL_CLASS(c)  (G_TYPE_CHECK_CLASS_TYPE((c), GQ_TYPE_BROWSER_MODEL))
#define GQ_BROWSER_MODEL_GET_CLASS(i) (G_TYPE_INSTANCE_GET_CLASS((i), GQ_TYPE_BROWSER_MODEL, GqBrowserModelClass))
    GType gq_browser_model_get_type(void);
GtkTreeModel *gq_browser_model_new(void);
void gq_browser_model_add_node(GqBrowserModel const *self,
                               GtkTreeIter const *iter, GqBrowserNode * node);
GqBrowserNode *gq_browser_model_get_node(GqBrowserModel const *self, GtkTreeIter const *iter);
gboolean gq_browser_model_validate(gconstpointer * self, GtkTreeIter const *iter);

enum
{
    GQ_BROWSER_MODEL_COL_NAME,
    GQ_BROWSER_MODEL_COL_TYPE,
    GQ_BROWSER_MODEL_N_COLUMNS
};

G_END_DECLS
#endif /* !GQ_BROWSER_MODEL_H */
