/* -*- mode: c-mode; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * vim: set expandtab:ts=4:sw=4
    GQ -- a GTK-based LDAP client
    Copyright (C) 1998-2003 Bert Vermeulen
    Copyright (C) 2002-2003 Peter Stamfest

    This program is released under the Gnu General Public License with
    the additional exemption that compiling, linking, and/or using
    OpenSSL is allowed.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

#ifndef GQ_MAINWIN_H_INCLUDED
#define GQ_MAINWIN_H_INCLUDED

#include <gtk/gtk.h>
#include "common.h"
#include "gq-stack.h"

G_BEGIN_DECLS typedef struct _GqWindow GqWindow;

G_END_DECLS
#include "gq-tab.h"
    G_BEGIN_DECLS
#define GQ_IS_WINDOW(i) (i != NULL)
    struct _GqWindow
{
    GtkWidget *mainwin;
    GtkWidget *mainbook;
    GtkWidget *statusbar;
    GtkWidget *filtermenu;
    GqStack *tab_history;

    /* the message log window */
    GtkWidget *ml_window;
    GtkWidget *ml_text;
    GtkTextBuffer *ml_buffer;
};

#warning "FIXME: remove this global variable"
extern GqWindow mainwin;

GqTab *gq_window_get_last_tab(GqWindow const *self, GType mode);
void go_to_page(GqTab * tab);
void enter_last_of_mode(GqTab * tab);

void cleanup(GqWindow * win);
void create_mainwin(GqWindow *);
void mainwin_update_filter_menu(GqWindow * win);

GqTab *new_modetab(GqWindow *, GType mode, gboolean has_focus);
void cleanup_all_tabs(GqWindow * win);
void cleanup_tab(GqTab * tab);
void update_serverlist(GqWindow * win);

/* gboolean ctrl_b_hack(GtkWidget *widget, GdkEventKey *event, gpointer obj); */

/* gboolean ctrl_w_hack(GtkWidget *widget, GdkEventKey *event, gpointer obj); */

/* return the GqTab for the n'th tab in the gq main window win */
GqTab *mainwin_get_tab_nth(GqWindow * win, int n);
GqTab *mainwin_get_current_tab(GtkWidget * notebook);

void message_log_append(const char *buf);

#define MESSAGE_LOG_MAX 1000

G_END_DECLS
#endif
