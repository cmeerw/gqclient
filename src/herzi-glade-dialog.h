/* -*- mode: c-mode; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * vim: set expandtab:ts=4:sw=4
 *
 * this file is part of gnome-gdb, a GUI for the gdb
 *
 * AUTHORS
 *       Sven Herzberg        <herzi@gnome-de.org>
 *
 * Copyright (C) 2005 Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef HERZI_GLADE_DIALOG_H
#define HERZI_GLADE_DIALOG_H

#include <gtk/gtkdialog.h>
#include <glade/glade-xml.h>

G_BEGIN_DECLS typedef struct _HerziGladeDialog HerziGladeDialog;
typedef struct _HerziGladeDialogClass HerziGladeDialogClass;

#define HERZI_TYPE_GLADE_DIALOG         (herzi_glade_dialog_get_type())
#define HERZI_GLADE_DIALOG(i)           (G_TYPE_CHECK_INSTANCE_CAST((i), HERZI_TYPE_GLADE_DIALOG, HerziGladeDialog))
#define HERZI_GLADE_DIALOG_CLASS(c)     (G_TYPE_CHECK_CLASS_CAST((c), HERZI_TYPE_GLADE_DIALOG, HerziGladeDialogClass))
#define HERZI_IS_GLADE_DIALOG(i)        (G_TYPE_CHECK_INSTANCE_TYPE((i), HERZI_TYPE_GLADE_DIALOG))
#define HERZI_IS_GLADE_DIALOG_CLASS(c)  (G_TYPE_CHECK_CLASS_TYPE((c), HERZI_TYPE_GLADE_DIALOG))
#define HERZI_GLADE_DIALOG_GET_CLASS(i) (G_TYPE_INSTANCE_GET_CLASS((i), HERZI_TYPE_GLADE_DIALOG, HerziGladeDialogClass))

GType herzi_glade_dialog_get_type(void);

struct _HerziGladeDialog
{
    GtkDialog base_instance;
};

struct _HerziGladeDialogClass
{
    GtkDialogClass base_class;

    gchar const *filename;
    gchar const *root_widget;

    /* vtable */
    void (*connect_widgets) (HerziGladeDialog * self, GladeXML * xml);
};

G_END_DECLS
#endif /* !HERZI_GLADE_DIALOG_H */
