/* -*- mode: c-mode; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * vim: set expandtab:ts=4:sw=4
 *
 * This file is part of GQ
 *
 * AUTHORS
 *     Sven Herzberg  <herzi@gnome-de.org>
 *
 * Copyright (C) 1998-2003 Bert Vermeulen
 * Copyright (C) 2002-2003 Peter Stamfest
 * Copyright (C) 2006      Sven Herzberg <herzi@gnome-de.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GQ_BROWSER_NODE_H
#define GQ_BROWSER_NODE_H

#include "gq-server.h"
#include "gq-tab.h"
#include "gq-tree-widget.h"

G_BEGIN_DECLS typedef GObject GqBrowserNode;
typedef struct _GqBrowserNodeClass GqBrowserNodeClass;

G_END_DECLS
#include "gq-tab-browse.h"      // for USE_TREE_VIEW
    G_BEGIN_DECLS
#define GQ_TYPE_BROWSER_NODE         (gq_browser_node_get_type())
#define GQ_BROWSER_NODE(i)           (G_TYPE_CHECK_INSTANCE_CAST((i), GQ_TYPE_BROWSER_NODE, GqBrowserNode))
#define GQ_BROWSER_NODE_CLASS(c)     (G_TYPE_CHECK_CLASS_CAST((c), GQ_TYPE_BROWSER_NODE, GqBrowserNodeClass))
#define GQ_IS_BROWSER_NODE(i)        (G_TYPE_CHECK_INSTANCE_TYPE((i), GQ_TYPE_BROWSER_NODE))
#define GQ_IS_BROWSER_NODE_CLASS(c)  (G_TYPE_CHECK_CLASS_TYPE((c), GQ_TYPE_BROWSER_NODE))
#define GQ_BROWSER_NODE_GET_CLASS(i) (G_TYPE_INSTANCE_GET_CLASS((i), GQ_TYPE_BROWSER_NODE, GqBrowserNodeClass))

/* Callback types to avoid a lot of warnings when assigning to the
   virtual function table */
    struct _GqBrowserNodeClass
{
    GObjectClass base_class;

    /* expansion callback of corresponding GtkCtreeNode */
    void (*expand) (GqBrowserNode * entry, int error_context,
#ifndef USE_TREE_VIEW
                    GQTreeWidget * ctreeroot, GQTreeWidgetNode * node, GqTab * tab
#else
                    GtkTreeModel * model, GtkTreeIter * iter, gpointer dummy G_GNUC_UNUSED
#endif
        );

    /* select callback of corresponding GtkCtreeNode */
    void (*select) (GqBrowserNode * entry, int error_context,
#ifndef USE_TREE_VIEW
                    GQTreeWidget * ctreeroot, GQTreeWidgetNode * node,
#else
                    GtkTreeModel * widget, GtkTreeIter * iter,
#endif
                    GqTab * tab);

    /* refresh callback of corresponding GtkCtreeNode */
    void (*refresh) (GqBrowserNode * entry,
                     int error_context, GtkTreeModel * model, GtkTreeIter * iter, GqTab * tab);

    gchar *(*get_name) (GqBrowserNode const *entry, gboolean long_form);

    /* implements the popup menu for the entry */
    void (*popup) (GqBrowserNode * entry,
                   GtkWidget * menu,
                   GtkTreeView * ctreeroot, GtkTreeIter * ctree_node, GqTab * tab);

    GqServer *(*get_server) (GqBrowserNode * self);
};

GType gq_browser_node_get_type(void);
gboolean gq_browser_node_get_seen(GqBrowserNode const *self);
void gq_browser_node_set_seen(GqBrowserNode * self, gboolean seen);
gchar *gq_browser_node_get_name(GqBrowserNode const *self, gboolean long_form);
GqServer *gq_browser_node_get_server(GqBrowserNode * self);

gchar const *gq_browser_node_get_status(GqBrowserNode const *self);
void gq_browser_node_set_status(GqBrowserNode * self, gchar const *type);

void gq_browser_node_expand(GqBrowserNode * entry, int error_context,
#ifndef USE_TREE_VIEW
                            GQTreeWidget * ctreeroot, GQTreeWidgetNode * node, GqTab * tab
#else
                            GtkTreeModel * model, GtkTreeIter * iter, gpointer dummy G_GNUC_UNUSED
#endif
    );
void gq_browser_node_popup(GqBrowserNode * entry,
                           GtkWidget * menu,
                           GtkTreeView * ctreeroot, GtkTreeIter * ctree_node, GqTab * tab);
void gq_browser_node_select(GqBrowserNode * self, int error_context,
#ifndef USE_TREE_VIEW
                            GQTreeWidget * ctreeroot, GQTreeWidgetNode * node,
#else
                            GtkTreeModel * widget, GtkTreeIter * iter,
#endif
                            GqTab * tab);

G_END_DECLS
#endif /* !GQ_BROWSER_NODE_H */
