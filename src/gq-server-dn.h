/* -*- mode: c-mode; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * vim: set expandtab:ts=4:sw=4
 *
 * This file is part of GQ
 *
 * AUTHORS
 *     Sven Herzberg  <herzi@gnome-de.org>
 *
 * Copyright (C) 2006  Sven Herzberg <herzi@gnome-de.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GQ_SERVER_DN_H
#define GQ_SERVER_DN_H

#include "gq-server.h"

G_BEGIN_DECLS typedef struct _GqServerDn GqServerDn;
typedef GObjectClass GqServerDnClass;

#define GQ_TYPE_SERVER_DN         (gq_server_dn_get_type())
#define GQ_SERVER_DN(i)           (G_TYPE_CHECK_INSTANCE_CAST((i), GQ_TYPE_SERVER_DN, GqServerDn))
#define GQ_SERVER_DN_CLASS(c)     (G_TYPE_CHECK_CLASS_CAST((c), GQ_TYPE_SERVER_DN, GqServerDnClass))
#define GQ_IS_SERVER_DN(i)        (G_TYPE_CHECK_INSTANCE_TYPE((i), GQ_TYPE_SERVER_DN))
#define GQ_IS_SERVER_DN_CLASS(c)  (G_TYPE_CHECK_CLASS_TYPE((c), GQ_TYPE_SERVER_DN))
#define GQ_SERVER_DN_GET_CLASS(i) (G_TYPE_INSTANCE_GET_CLASS((i), GQ_TYPE_SERVER_DN, GqServerDnClass))

GType gq_server_dn_get_type(void);

GqServerDn *gq_server_dn_new(gchar const *dn, GqServer * server);

gchar const *gq_server_dn_get_dn(GqServerDn const *self);
gchar *const *gq_server_dn_get_object_class(GqServerDn * self);
GqServer *gq_server_dn_get_server(GqServerDn const *self);

struct _GqServerDn
{
    GObject base_instance;
    int flags;                  /* used to specify more
                                 * information if needed */
};

G_END_DECLS
#endif /* !GQ_SERVER_DN_H */
