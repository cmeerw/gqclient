/* -*- mode: c-mode; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * vim: set expandtab:ts=4:sw=4
 *
 * This file is part of GQ
 *
 * AUTHORS
 *     Sven Herzberg  <herzi@gnome-de.org>
 *
 * Copyright (C) 2006  Sven Herzberg <herzi@gnome-de.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "gq-browser-node.h"

#include <string.h>
#include <glib/gi18n.h>

struct GqBrowserNodePrivate
{
    gboolean seen;
    gchar *status;
};
#define P(i) (G_TYPE_INSTANCE_GET_PRIVATE((i), GQ_TYPE_BROWSER_NODE, struct GqBrowserNodePrivate))

/* Gets the user visible name of the node, must be freed using g_free */
gchar *gq_browser_node_get_name(GqBrowserNode const *self, gboolean long_form)
{
    g_return_val_if_fail(GQ_IS_BROWSER_NODE(self), NULL);
    g_return_val_if_fail(GQ_BROWSER_NODE_GET_CLASS(self)->get_name, NULL);

    return GQ_BROWSER_NODE_GET_CLASS(self)->get_name(self, long_form);
}

gboolean gq_browser_node_get_seen(GqBrowserNode const *self)
{
    g_return_val_if_fail(GQ_IS_BROWSER_NODE(self), FALSE);

    return P(self)->seen;
}

void gq_browser_node_set_seen(GqBrowserNode * self, gboolean seen)
{
    g_return_if_fail(GQ_IS_BROWSER_NODE(self));

    if(P(self)->seen == seen)
    {
        return;
    }

    P(self)->seen = seen;

    g_object_notify(G_OBJECT(self), "seen");
}

GqServer *gq_browser_node_get_server(GqBrowserNode * self)
{
    g_return_val_if_fail(GQ_IS_BROWSER_NODE(self), NULL);
    g_return_val_if_fail(GQ_BROWSER_NODE_GET_CLASS(self)->get_server, NULL);

    return GQ_BROWSER_NODE_GET_CLASS(self)->get_server(self);
}

gchar const *gq_browser_node_get_status(GqBrowserNode const *self)
{
    g_return_val_if_fail(GQ_IS_BROWSER_NODE(self), NULL);

    return P(self)->status;
}

void gq_browser_node_set_status(GqBrowserNode * self, gchar const *status)
{
    g_return_if_fail(GQ_IS_BROWSER_NODE(self));

    if(P(self)->status == status || (P(self)->status && status && !strcmp(P(self)->status, status)))
    {
        return;
    }

    g_free(P(self)->status);
    P(self)->status = g_strdup(status);

    g_object_notify(G_OBJECT(self), "status");
}

void gq_browser_node_expand(GqBrowserNode * self, int error_context,
#ifndef USE_TREE_VIEW
                            GQTreeWidget * ctreeroot, GQTreeWidgetNode * node, GqTab * tab
#else
                            GtkTreeModel * model, GtkTreeIter * iter, gpointer dummy G_GNUC_UNUSED
#endif
    )
{
    g_return_if_fail(GQ_IS_BROWSER_NODE(self));
    g_return_if_fail(GQ_BROWSER_NODE_GET_CLASS(self)->expand);

    GQ_BROWSER_NODE_GET_CLASS(self)->expand(self, error_context,
#ifndef USE_TREE_VIEW
                                            ctreeroot, node, tab
#else
                                            model, iter, dummy
#endif
        );
}

void
gq_browser_node_popup(GqBrowserNode * entry,
                      GtkWidget * menu,
                      GtkTreeView * ctreeroot, GtkTreeIter * ctree_node, GqTab * tab)
{
    g_return_if_fail(GQ_IS_BROWSER_NODE(entry));
    g_return_if_fail(GQ_BROWSER_NODE_GET_CLASS(entry)->popup);

    GQ_BROWSER_NODE_GET_CLASS(entry)->popup(entry, menu, ctreeroot, ctree_node, tab);
}

void gq_browser_node_select(GqBrowserNode * self, int error_context,
#ifndef USE_TREE_VIEW
                            GQTreeWidget * model, GQTreeWidgetNode * iter,
#else
                            GtkTreeModel * model, GtkTreeIter * iter,
#endif
                            GqTab * tab)
{
    g_return_if_fail(GQ_IS_BROWSER_NODE(self));
    g_return_if_fail(GQ_BROWSER_NODE_GET_CLASS(self)->select);

    GQ_BROWSER_NODE_GET_CLASS(self)->select(self, error_context, model, iter, tab);
}

/* GType */
G_DEFINE_ABSTRACT_TYPE(GqBrowserNode, gq_browser_node, G_TYPE_OBJECT);

enum
{
    PROP_0,
    PROP_SEEN,
    PROP_STATUS
};

static void gq_browser_node_init(GqBrowserNode * self G_GNUC_UNUSED)
{
}

static void gq_browser_node_class_init(GqBrowserNodeClass * self_class)
{
    GObjectClass *object_class = G_OBJECT_CLASS(self_class);
    g_object_class_install_property(object_class,
                                    PROP_SEEN,
                                    g_param_spec_boolean("seen",
                                                         _("Seen"),
                                                         _("Has this node already been looked at"),
                                                         FALSE, 0));
#warning "FIXME: add G_PARAM_READWRITE"
    g_object_class_install_property(object_class,
                                    PROP_STATUS,
                                    g_param_spec_string("status",
                                                        _("The Node's Status"),
                                                        _
                                                        ("The status of the node (eg. connected, disconnected for a server)"),
                                                        NULL, 0));
#warning "FIXME: add G_PARAM_READWRITE"

    /* GqBrowserNodeClass */
    g_type_class_add_private(self_class, sizeof(struct GqBrowserNodePrivate));
}
