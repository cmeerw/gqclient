/* -*- mode: c-mode; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * vim: set expandtab:ts=4:sw=4
 *
 * This file is part of GQ
 *
 * AUTHORS
 *     Sven Herzberg  <herzi@gnome-de.org>
 *
 * Copyright (C) 2006  Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#include "gq-main-loop.h"

#include <glib/gi18n.h>
#include <gtk/gtkmain.h>

static gulong blocks = 0;

void gq_main_loop_block(void)
{
    blocks++;
}

gboolean gq_main_loop_blocked(void)
{
    return blocks > 0;
}

void gq_main_loop_release(void)
{
    blocks--;
}

void gq_main_loop_flush(void)
{
    if(G_LIKELY(!blocks))
    {
        while(gtk_events_pending())
        {
            gtk_main_iteration();
        }
    }
}
