/* -*- mode: c-mode; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * vim: set expandtab:ts=4:sw=4
 *
 * This file is part of GQ
 *
 * AUTHORS
 *     Sven Herzberg  <herzi@gnome-de.org>
 *
 * Copyright (C) 2006  Sven Herzberg <herzi@gnome-de.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GQ_LOGIN_DIALOG_H
#define GQ_LOGIN_DIALOG_H

#include "gq-server.h"
#include "herzi-glade-dialog.h"

G_BEGIN_DECLS typedef HerziGladeDialog GqLoginDialog;
typedef HerziGladeDialogClass GqLoginDialogClass;

#define GQ_TYPE_LOGIN_DIALOG         (gq_login_dialog_get_type())
#define GQ_LOGIN_DIALOG(i)           (G_TYPE_CHECK_INSTANCE_CAST((i), GQ_TYPE_LOGIN_DIALOG, GqLoginDialog))
#define GQ_LOGIN_DIALOG_CLASS(c)     (G_TYPE_CHECK_CLASS_CAST((c), GQ_TYPE_LOGIN_DIALOG, GqLoginDialogClass))
#define GQ_IS_LOGIN_DIALOG(i)        (G_TYPE_CHECK_INSTANCE_TYPE((i), GQ_TYPE_LOGIN_DIALOG))
#define GQ_IS_LOGIN_DIALOG_CLASS(c)  (G_TYPE_CHECK_CLASS_TYPE((c), GQ_TYPE_LOGIN_DIALOG))
#define GQ_LOGIN_DIALOG_GET_CLASS(i) (G_TYPE_INSTANCE_GET_CLASS((i), GQ_TYPE_LOGIN_DIALOG, GqLoginDialogClass))

GType gq_login_dialog_get_type(void);
GtkWidget *gq_login_dialog_new(GqServer * server);
gchar const *gq_login_dialog_get_password(GqLoginDialog const *self);
gboolean gq_login_dialog_get_save_password(GqLoginDialog const *self);

G_END_DECLS
#endif /* !GQ_LOGIN_DIALOG_H */
