/* -*- mode: c-mode; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * vim: set expandtab:ts=4:sw=4
 *
 * This file is part of GQ
 *
 * AUTHORS
 *     Sven Herzberg  <herzi@gnome-de.org>
 *
 * Copyright (C) 2006  Sven Herzberg <herzi@gnome-de.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GQ_SERVER_MODEL_H
#define GQ_SERVER_MODEL_H

#include <gtk/gtktreemodel.h>
#include "gq-server-list.h"

G_BEGIN_DECLS typedef GObject GqServerModel;
typedef struct _GqServerModelClass GqServerModelClass;

#define GQ_TYPE_SERVER_MODEL         (gq_server_model_get_type())
#define GQ_SERVER_MODEL(i)           (G_TYPE_CHECK_INSTANCE_CAST((i), GQ_TYPE_SERVER_MODEL, GqServerModel))
#define GQ_SERVER_MODEL_CLASS(c)     (G_TYPE_CHECK_CLASS_CAST((c), GQ_TYPE_SERVER_MODEL, GqServerModelClass))
#define GQ_IS_SERVER_MODEL(i)        (G_TYPE_CHECK_INSTANCE_TYPE((i), GQ_TYPE_SERVER_MODEL))
#define GQ_IS_SERVER_MODEL_CLASS(c)  (G_TYPE_CHECK_CLASS_TYPE((c), GQ_TYPE_SERVER_MODEL))
#define GQ_SERVER_MODEL_GET_CLASS(i) (G_TYPE_INSTANCE_GET_CLASS((i), GQ_TYPE_SERVER_MODEL, GqServerModelClass))

GType gq_server_model_get_type(void);

GtkTreeModel *gq_server_model_new(GqServerList * list);
gboolean gq_server_model_get_iter(GqServerModel const *self, GtkTreeIter * iter, GqServer * server);
GqServer *gq_server_model_get_server(GqServerModel const *self, GtkTreeIter * iter);

enum
{
    GQ_SERVER_MODEL_COL_NAME,
    GQ_SERVER_MODEL_COL_STATUS,
    GQ_SERVER_MODEL_N_COLUMNS
};

G_END_DECLS
#endif /* !GQ_SERVER_MODEL_H */
