/* -*- mode: c-mode; tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 4 -*-
 * vim: set expandtab:ts=4:sw=4
 *
 * This file is part of GQ
 *
 * AUTHORS
 *     Sven Herzberg  <herzi@gnome-de.org>
 *
 * Copyright (C) 2006  Sven Herzberg
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation; either version 2.1 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA 02111-1307
 * USA
 */

#ifndef GQ_DIFF_TAB_H
#define GQ_DIFF_TAB_H

#include "gq-server-dn.h"
#include "gq-tab.h"

G_BEGIN_DECLS typedef GqTab GqDiffTab;
typedef GqTabClass GqDiffTabClass;

#define GQ_TYPE_DIFF_TAB         (gq_diff_tab_get_type())
#define GQ_DIFF_TAB(i)           (G_TYPE_CHECK_INSTANCE_CAST((i), GQ_TYPE_DIFF_TAB, GqDiffTab))
#define GQ_DIFF_TAB_CLASS(c)     (G_TYPE_CHECK_CLASS_CAST((c), GQ_TYPE_DIFF_TAB, GqDiffTabClass))
#define GQ_IS_DIFF_TAB(i)        (G_TYPE_CHECK_INSTANCE_TYPE((i), GQ_TYPE_DIFF_TAB))
#define GQ_IS_DIFF_TAB_CLASS(c)  (G_TYPE_CHECK_CLASS_TYPE((c), GQ_TYPE_DIFF_TAB))
#define GQ_DIFF_TAB_GET_CLASS(i) (G_TYPE_INSTANCE_GET_CLASS((i), GQ_TYPE_DIFF_TAB, GqDiffTabClass))

GType gq_diff_tab_get_type(void);
void gq_diff_tab_set_entry(GqDiffTab * self, gint position, GqServerDn * entry);

G_END_DECLS
#endif /* !GQ_DIFF_TAB_H */
