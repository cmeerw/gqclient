#!/bin/bash                                          
 
for i in $(find src/ -regex ".*\\.[ch]")
do
	echo "Reindenting $i..."
	indent $i
	rm $i~
	if head -n 1 $i | grep "^\s*$" > /dev/null
	then
		tail -n+2 $i > $i.tmp
		mv $i.tmp $i
	fi
done

